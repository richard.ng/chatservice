# Chat Events Interface Service

## Background

This service is used for interfacing different chat services, e.g. Slack, Discord, Keybase, and polling events from those services to be routed to a message broker, e.g. Kafka or RabbitMq.
