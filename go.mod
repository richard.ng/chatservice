module gitlab.com/richard.ng/chatservice

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	github.com/kelseyhightower/envconfig v1.4.0
	go.uber.org/zap v1.15.0
)
